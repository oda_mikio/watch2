<?php

mb_language("uni");
mb_internal_encoding("utf-8"); //内部文字コードを変更
mb_http_input("auto");
mb_http_output("utf-8");
header("Content-type:application/json");

include 'config.php';
$usage = "http://domain.this/path/get.php?db=dbname&collection=collname";

try {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_GET['db'])) {
            if (isset($_GET['collection'])) {
                $m = new Mongo($MongoAddress);
                $db = $m->selectDB($_GET['db']);
                $data = $db->selectCollection($_GET['collection']);
                
                $ID = new MongoId($_POST['id']);

                $query = array("_id" => $ID);
                $item = $data->findOne($query);
                if ($item != null) {
                    $item["id"] = $item["_id"]->__toString();
                    unset($item["_id"]);
                    $result = json_encode($item);
                }
             
                echo $result;
            } else {
                echo urldecode('{"result":"collection"}');
            }
        } else {
            echo urldecode('{"result":"db"}');
        }
    } else {
        echo urldecode('{"result":"POST"}');
    }
} catch (Exception $e) {
   echo urldecode('{"result":"' . $e->getMessage() . '"}');
}
?>