<?php

//  upload.php?device= &message= &width= &height= &quality=
mb_language("uni");
mb_internal_encoding("utf-8"); //内部文字コードを変更
mb_http_input("auto");
mb_http_output("utf-8");
header("Content-type:application/xml");

include 'config.php';

try {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_GET['filename'])) {
            $m = new Mongo($MongoAddress);
            $db = $m->selectDB($_GET['db']);

            $grid = $db->getGridFS();

            $query = array("filename" => $_GET['filename']);
            $photo = $grid->findOne($query);
            if ($photo != null) {
                $grid->remove($photo, array("safe" => true));
            }
        } else {
            echo urldecode('{"result":"1"}');
        }
    } else {
        echo urldecode('{"result":"POST"}');
    }
} catch (Exception $e) {
    echo urldecode('{"result":"' . $e->getMessage() . '"}');
}

?>