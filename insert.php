<?php

mb_language("uni");
mb_internal_encoding("utf-8"); //内部文字コードを変更
mb_http_input("auto");
mb_http_output("utf-8");
header("Content-type:application/json");

include 'config.php';
$usage = "http://domain.this/path/insert.php?db=dbname&collection=collname";

try {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_GET['db'])) {
            if (isset($_GET['collection'])) {
                $m = new Mongo($MongoAddress);
                $db = $m->selectDB($_GET['db']);
                $data = $db->selectCollection($_GET['collection']);

                $item = json_decode((urldecode($_POST['data'])), true);
                $item['timestamp'] = time();
                unset($item["id"]);                         //unset String ID for save 

                $data->insert($item, array("safe" => true));

                $item["id"] = $item["_id"]->__toString();   //True ID to String ID for return
                unset($item["_id"]);                        //unset True ID for return
                echo urldecode(json_encode($item));
            } else {
                echo urldecode('{"result":"collection"}');
            }
        } else {
            echo urldecode('{"result":"db"}');
        }
    } else {
        echo urldecode('{"result":"POST"}');
    }
} catch (Exception $e) {
    echo urldecode('{"result":"' . $e->getMessage() . '"}');
}
?>