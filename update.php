<?php

mb_language("uni");
mb_internal_encoding("utf-8"); //内部文字コードを変更
mb_http_input("auto");
mb_http_output("utf-8");
header("Content-type:application/json");

include 'config.php';
$usage = "http://domain.this/path/update.php?db=dbname&collection=collname";

try {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_GET['db'])) {
            if (isset($_GET['collection'])) {
                $m = new Mongo($MongoAddress);
                $db = $m->selectDB($_GET['db']);
                $data = $db->selectCollection($_GET['collection']);

                $receiveitem = json_decode(urldecode($_POST['data']), true);
                $id = new MongoId($receiveitem['id']);
                unset($receiveitem['id']);

                $query = array("_id" => $id);
                $item = $data->findOne($query);
                if ($item != null) {
                    foreach ($receiveitem as $key => $value) {
                        $item[$key] = $value;
                    }

                    $data->save($item, array("safe" => true));

                    $item["id"] = $item["_id"]->__toString();
                    unset($item["_id"]);
                    echo urldecode(json_encode($item));
                } else {
                    echo urldecode('{"result":"not found"}');
                }
            } else {
                echo urldecode('{"result":"collection"}');
            }
        } else {
            echo urldecode('{"result":"db"}');
        }
    } else {
        echo urldecode('{"result":"POST"}');
    }
} catch (Exception $e) {
   echo urldecode('{"result":"' . $e->getMessage() . '"}');
}
?>