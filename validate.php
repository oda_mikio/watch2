<?php

mb_language("uni");
mb_internal_encoding("utf-8"); //内部文字コードを変更
mb_http_input("auto");
mb_http_output("utf-8");
header("Content-type:text/plain");

try {
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        echo urldecode('ok');
    } else {
        echo urldecode('error');
    }
    
    /////////////
} catch (Exception $e) {
    echo urldecode($e->getMessage());
}
?>