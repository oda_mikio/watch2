<?php

mb_language("uni");
mb_internal_encoding("utf-8"); //内部文字コードを変更
mb_http_input("auto");
mb_http_output("utf-8");
header("Content-type:application/json");

include 'config.php';
$usage = "http://domain.this/path/delete.php?db=dbname&collection=collname";

try {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_GET['db'])) {
            if (isset($_GET['collection'])) {
                $m = new Mongo($MongoAddress);
                $db = $m->selectDB($_GET['db']);
                $data = $db->selectCollection($_GET['collection']);

                $receiveitem = json_decode(urldecode($_POST['data']), true);
                $id = new MongoId($receiveitem['id']);
                unset($receiveitem['id']);

                $query = array("_id" => $id);
                $item = $data->findOne($query);
                if ($item != null) {
                    $data->remove($item, array("safe" => true));
                    echo urldecode('{"result":"ok"}');
                } else {
                    echo urldecode('{"result":"not found"}');
                }
            } else {
                echo urldecode('{"result":"collection"}');
            }
        } else {
            echo urldecode('{"result":"db"}');
        }
    } else {
        echo urldecode('{"result":"POST"}');
    }
} catch (Exception $e) {
   echo urldecode('{"result":"' . $e->getMessage() . '"}');
}
?>