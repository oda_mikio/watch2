<?php

mb_language("uni");
mb_internal_encoding("utf-8"); //内部文字コードを変更
mb_http_input("auto");
mb_http_output("utf-8");
header("Content-type:application/json");

include 'config.php';

function scan(&$a) {
    foreach ($a as $key => &$val) {
        if (is_array($val)) {
            scan($val);
        }

        if (strlen($val) >= 3) {
            if ($val[0] == "/") {
                if (mb_substr($val, -1) == "/") {
                    $val = new MongoRegex($val);
                }
            }
        }
    }
}

try {
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        $m = new Mongo($MongoAddress);
        $db = $m->selectDB("watch2");
        $data = $db->selectCollection("master");

        $aaaa = stripslashes(urldecode(urlencode('{"productNo" : "/\d{5}/"}')));
        
    //   echo $aaaa;
        $query = json_decode($aaaa, true);
        
        
        scan($query);
echo $query;

       $item['result'] = $data->find($query)->count();

  //      echo json_encode($item);
    } else {
        echo urldecode('{"result":"POST"}');
    }
} catch (Exception $e) {
    echo urldecode('{"result":"' . $e->getMessage() . '"}');
}

echo json_encode($a);
?>