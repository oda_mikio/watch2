<?php

mb_language("uni");
mb_internal_encoding("utf-8"); //内部文字コードを変更
mb_http_input("auto");
mb_http_output("utf-8");
header("Content-type:application/json");

include 'config.php';
$usage = "http://domain.this/path/query.php?db=dbname&collection=collname&from=from&count=count";

function scan(&$a) {
    foreach ($a as $key => &$val) {
        if (is_array($val)) {
            scan($val);
        }

        if (mb_strlen($val) >= 3) {
            if ($val[0] == "/") {
                if (mb_substr($val,-1) == "/") {
                    $val = new MongoRegex($val);
                }
            }
        }
    }
}

try {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_GET['db'])) {
            if (isset($_GET['collection'])) {
                $m = new Mongo($MongoAddress);
                $db = $m->selectDB($_GET['db']);
                $data = $db->selectCollection($_GET['collection']);

                $query = json_decode(stripslashes(urldecode($_POST['query'])), true);
          //      scan($query);

                $item['result'] = $data->find($query)->count();

                echo json_encode($item);
            } else {
                echo urldecode('{"result":"collection"}');
            }
        } else {
            echo urldecode('{"result":"db"}');
        }
    } else {
        echo urldecode('{"result":"POST"}');
    }
} catch (Exception $e) {
    echo urldecode('{"result":"' . $e->getMessage() . '"}');
}
?>