<?php

include 'config.php';


try {
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
        if (isset($_GET['filename'])) {
            $m = new Mongo($MongoAddress);
            $db = $m->selectDB($_GET['db']);

            $grid = $db->getGridFS();

            $query = array("filename" => $_GET['filename']);
            $photo = $grid->findOne($query);
            if ($photo != null) {
                if (isset($_GET['type'])) {
                    header('Content-type: ' + $_GET['type']);
                } else {
                    header('Content-type: image/jpeg');
                }
                echo $photo->getBytes();
            }
        }
    }
} catch (Exception $e) {
    echo urldecode('{"result":"' . $e->getMessage() . '"}');
}
?>