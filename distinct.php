<?php

mb_language("uni");
mb_internal_encoding("utf-8"); //内部文字コードを変更
mb_http_input("auto");
mb_http_output("utf-8");
header("Content-type:application/json");

include 'config.php';

try {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_GET['db'])) {
            if (isset($_GET['collection'])) {
                $m = new Mongo($MongoAddress);
                $db = $m->selectDB($_GET['db']);
                $query = json_decode(stripslashes(urldecode($_POST['query'])), true);
            //    scan($query);
                $result = $db->command(array('distinct' => $_GET['collection'], 'key' => $_GET['key'], 'query' => $query));

                echo json_encode($result);
            } else {
                echo urldecode('{"result":"collection"}');
            }
        } else {
            echo urldecode('{"result":"db"}');
        }
    } else {
        echo urldecode('{"result":"POST"}');
    }
} catch (Exception $e) {
    echo urldecode('{"result":"' . $e->getMessage() . '"}');
}
?>