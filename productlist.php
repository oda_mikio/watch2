<?PHP
mb_language("uni");
mb_internal_encoding("utf-8"); //内部文字コードを変更
mb_http_input("auto");
mb_http_output("utf-8");
header("Content-type:text/html");
?>
<HTML>
    <HEAD>
        <meta charset="utf-8">
        <style type="text/css">
            div {
                font-family:Helvetica Neue, Arial, sans-serif;
                font-size: 9px; 
            }
            .main,tr,td { border: 2px #2b2b2b solid; }
            .no {font-size: 18px;  }
            .spec {font-size: 8px; border: 0px #2b2b2b solid; }
            .description {width:200px}
            .description2 {width:200px}
        </style>
    </HEAD>
    <BODY>
        <?php
        include 'config.php';
        try {
            if ($_SERVER["REQUEST_METHOD"] == "GET") {


                $m = new Mongo($MongoAddress);
                $db = $m->watch2;
                $data = $db->master;
                $grid = $db->getGridFS();
                $haswindow = false;
                $hascount = false;


                if (isset($_GET['from'])) {
                    $from = intval($_GET['from']);
                    $haswindow = true;
                }


                if (isset($_GET['count'])) {
                    $count = intval($_GET['count']);
                    $hascount = true;
                }


                $cursor = $data->find();

                if ($haswindow) {
                    $cursor = $cursor->skip($from);
                }
                if ($hascount) {
                    $cursor = $cursor->limit($count);
                }
                echo '<table class="main">';

                while ($cursor->hasNext()) {
                    $item = $cursor->getNext();
                    echo '<tr>';
                    echo '<td>';
                    echo "<div class='no'>" . $item["productNo"] . "</div>";
                    echo '</td>';

                    echo '<td>';
                    echo "<div>" . $item["productName"] . "</div>";
                    echo '</td>';

                    echo "<td class='description'>";
                    echo "<div>" . $item["brandDescription"] . "</div>";
                    echo '</td>';

                    echo "<td class='description2'>";
                    echo "<div>" . $item["descriptionOfItem"] . "</div>";
                    echo '</td>';

                    echo '<td>';
                    echo '<table class="spec">';
                    if ($item["spec"] != null) {
                       
                        foreach ($item["spec"] as $array) {
                            
                            echo '<tr>';
                            echo '<td>' . $array["Key"] . '</td>';
                            echo '<td>' . $array["Value"] . '</td>';
                            echo '</tr>';
                        }
                        
                    }
                    echo '</table>';
                    echo '</td>';

                    if ($item["pictures"] != null) {

                        for ($index = 0; $index < 5; $index++) {
                            echo '<td>';
                            if ($item["pictures"][$index] != null) {
                                echo '<img src="http://gourmet-off.com/watch2/get_picture.php?db=watch2&filename=' . $item["pictures"][$index] . '" height="64" width="64" />';
                            }
                            echo '</td>';
                        }
                    }
                    echo '</tr>';
                }
                echo '</table>';
            } else {
                echo urldecode('{"result":"POST"}');
            }
        } catch (Exception $e) {
            echo urldecode('{"result":"' . $e->getMessage() . '"}');
        }
        ?>
    </BODY>
</HTML>