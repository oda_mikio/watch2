<?php

mb_language("uni");
mb_internal_encoding("utf-8"); //内部文字コードを変更
mb_http_input("auto");
mb_http_output("utf-8");
header("Content-type:application/json");

include 'config.php';
$usage = "http://domain.this/path/query.php?db=dbname&collection=collname&from=from&count=count";

function scan(&$a) {
    foreach ($a as $key => &$val) {
        if (is_array($val)) {
            scan($val);
        }

        if (mb_strlen($val) >= 3) {
            if ($val[0] == "/") {
                if (mb_substr($val,-1) == "/") {
                    $val = new MongoRegex($val);
                }
            }
        }
    }
}

try {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_GET['db'])) {
            if (isset($_GET['collection'])) {
                $m = new Mongo($MongoAddress);
                $db = $m->selectDB($_GET['db']);
                $data = $db->selectCollection($_GET['collection']);
                $haswindow = false;
                $hascount = false;

                $from = 0;
                if (isset($_GET['from'])) {
                    $from = intval($_GET['from']);
                    $haswindow = true;
                }

                $count = 10;
                if (isset($_GET['count'])) {
                    $count = intval($_GET['count']);
                    $hascount = true;
                }

                $result = "[";
                $delimmiter = "";

                $query = json_decode(stripslashes(urldecode($_POST['query'])), true);
             //   scan($query);
                $sort = json_decode(stripslashes(urldecode($_POST['sort'])), true);
                $cursor = $data->find($query)->sort($sort);

                if ($haswindow) {
                    $cursor = $cursor->skip($from);
                }
                if ($hascount) {
                    $cursor = $cursor->limit($count);
                }

                while ($cursor->hasNext()) {
                    $item = $cursor->getNext();

                    $item["id"] = $item["_id"]->__toString();
                    unset($item["_id"]);
                    $result .= $delimmiter . json_encode($item);

                    $delimmiter = ",";
                }
                $result .= "]";
                echo $result;
            } else {
                echo urldecode('{"result":"collection"}');
            }
        } else {
            echo urldecode('{"result":"db"}');
        }
    } else {
        echo urldecode('{"result":"POST"}');
    }
} catch (Exception $e) {
   echo urldecode('{"result":"' . $e->getMessage() . '"}');
}
?>